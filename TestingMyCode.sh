#!/bin/bash

announce()
{
	echo -e "\n\n\n"
	echo "=============================================="
	echo "Compiling '$1'"
	echo "=============================================="
}

announce "./Testing Code/BasicArithmatic.GL"
java -classpath ./bin GraphLab "WrittenFile.json" < "./Testing Code/BasicArithmatic.GL"
cat ./WrittenFile.json

announce "./Testing Code/NestedFunctions.GL"
java -classpath ./bin GraphLab "WrittenFile.json" < "./Testing Code/NestedFunctions.GL"
cat ./WrittenFile.json

announce "./Testing Code/AggregateFunctions.GL"
java -classpath ./bin GraphLab "WrittenFile.json" < "./Testing Code/AggregateFunctions.GL"
cat ./WrittenFile.json

announce "./Testing Code/ScalarFunctions.GL"
java -classpath ./bin GraphLab "WrittenFile.json" < "./Testing Code/ScalarFunctions.GL"
cat ./WrittenFile.json

announce "./Testing Code/ListFunctions.GL"
java -classpath ./bin GraphLab "WrittenFile.json" < "./Testing Code/ListFunctions.GL"
cat ./WrittenFile.json
