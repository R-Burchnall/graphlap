package uk.ac.derby.ldi.graphlab.compiler;

import uk.ac.derby.ldi.graphlab.parser.ast.ASTCode;
import uk.ac.derby.ldi.graphlab.parser.ast.GraphLab;
import uk.ac.derby.ldi.graphlab.parser.ast.GraphLabVisitor;

import java.io.*;
import java.beans.XMLEncoder;

public class Compiler {
	
	private static void usage() {
		System.out.println("Usage: silc [-d0 | -d1 | -f] <output filename> < <source>");
		System.out.println("          -d0 -- run-time debugging");
		System.out.println("          -d1 -- output AST");
		System.out.println("          -f  -- Read in from pre-determined file (Debugging)");
	}
	
	private static boolean isValidFilename(String s) {
		if(s.matches("[-_.A-Za-z0-9]+")) {
			return true;
		}
		return false;
	}
	
	public static void main(String args[]) {
		boolean debugOnRun = false;
		boolean debugAST = false;
		boolean readFromFile = false;
		String outputFilename = "";

		for(String s : args) {
			switch (s) {
				case "-d0":
					debugOnRun = true;
					break;
				case "-d1":
					debugAST = true;
					break;
				case "-f":
					readFromFile = true;
					break;
				default:
					if(s.length() > 0 && isValidFilename(s)) {
						outputFilename = s;
						continue;
					}
					usage();
					return;
			}
		}
		
		if(outputFilename.isEmpty()) {
			usage();
			return;
		}
		System.out.println("Compiling output into file: " + outputFilename);
		
		GraphLab language = new GraphLab(readFromFile ? readFile() : System.in);
		try {
			ASTCode parser = language.code();
			GraphLabVisitor nodeVisitor;
			if (debugAST)
				nodeVisitor = new ParserDebugger();
			else {
				if (debugOnRun)
					System.out.println("Compiling...");
				nodeVisitor = new Parser();
			}
			// Run the input stream through through parser/compiler.  Obtain an executable ValueOperator as a result.
			Parser.SetOutputFilename(outputFilename);
			parser.jjtAccept(nodeVisitor, null);

			
		} catch (Throwable e) {
			System.out.println("There has been an error:" + e.getMessage());
		}
	}
	
	private static InputStream readFile() {
		
		try {
			return new BufferedInputStream(new FileInputStream(new File("Testing Code/ListFunctions.GL")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		throw new IllegalStateException("could not read file");
	}
}
