package uk.ac.derby.ldi.graphlab.compiler;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.*;

import uk.ac.derby.ldi.graphlab.values.Operand;

public class DataProcessor {
	
	private String path = "";
	private String outputFilename = "";
	private JSONObject jo = null;
	private Map<String, Operand> vars = new HashMap<String, Operand>();
	
	//Initialisation, allows path assignment to reduce number of method calls.
	public DataProcessor(String path, String outputFilename) {
		SetPath(path);
		this.outputFilename = outputFilename;
	}
	
	//Returns current path.
	public String GetPath() {
		return this.path;
	}
	
	//Sets the path used to access the json file, deals with relative pathing.
	public void SetPath(String path) {
		String dir = ""; System.getProperty("user.dir");
		path = path.substring(1, path.length()-1); // Removing " "
		
		//Test for relative path to convert to full path
		if(path.charAt(0) == '.') {
			dir = System.getProperty("user.dir");
			path = path.substring(1);
		}
		this.path = dir+path;
	}
	
	//Returns array of floats
	public Operand getVariable(String Key){
		return vars.get(Key);
	}
	
	//Allows new variables to be initalised
	public void setVariable(String key, Operand values) {
		vars.put(key, values);	
	}
	
	//Loads data from file and stores this data into a hashmap.
	public void LoadData(String[] Keys, String[] variables)
	{
		String fileText = ReadFile(path);
		if(fileText.length() < 2) {
			System.out.println("Cannot continue, couldn't read file.");
			return;
		}
		
		try {
			jo = new JSONObject(fileText);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		for(int i = 0; i < variables.length; i++){
			List<Float> values = new ArrayList<Float>();
			for(int j = 0; j < jo.getJSONArray(variables[i]).length(); j++) {
				values.add(jo.getJSONArray(variables[i]).getFloat(j));
			}
			Operand o = new Operand(values);
			this.vars.put(Keys[i], o);
		}
		System.out.println(this.vars.get("ValuesX"));
	}
	
	//Reads file and returns file text as a string.
	private String ReadFile(String path) {
		InputStream is = null;
		try {
			System.out.println("Attempting to read from path: " + path);
			is = new FileInputStream(path);
		} catch (FileNotFoundException e) {
			System.out.println("File couldn't be found: " + path);
			return "";
		}
	
		BufferedReader buf = new BufferedReader(new InputStreamReader(is));
		        
		String line = "";
		StringBuilder sb = new StringBuilder();
		        
		while(line != null){
		   try {
			   line = buf.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		   
		   if(line != null)
		   		sb.append(line).append("\n");
		}
		        
		return sb.toString();
	}
	
	private void WriteFile(String filename, String contents) {
		String dir = System.getProperty("user.dir");
		try {
			FileWriter fw = new FileWriter(dir + "/" + filename);
			fw.write(contents);
			fw.flush();
			fw.close();
			System.out.println("Wrote to file: " + dir + "/" + filename);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	public void write(List<String> keys) {
		JSONObject toWrite = new JSONObject();
		for (String k : keys) {
			toWrite.put(k, vars.get(k).values);
		}
		
		WriteFile(outputFilename, toWrite.toString(1));
	}
}
