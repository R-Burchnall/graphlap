package uk.ac.derby.ldi.graphlab.compiler;

import java.util.List;

public class FunctionPayload {
	private String name;
	private List<String> variables;

	public FunctionPayload(String name, List<String> variables) {
		this.name = name;
		this.variables = variables;
	}
	
	public String getName() {
		return name;
	}
	
	public List<String> getVariable(){
		return variables;
	}
}
