package uk.ac.derby.ldi.graphlab.compiler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import uk.ac.derby.ldi.graphlab.operations.*;
import uk.ac.derby.ldi.graphlab.parser.ast.*;
import uk.ac.derby.ldi.graphlab.values.Operand;

public class Parser implements GraphLabVisitor {
	
	// Code generator
	private DataProcessor processor;
	private static String outputFilename;
	
	/** Ctor */
	public Parser() {
	}
	
	public static void SetOutputFilename(String s) {
		outputFilename = s;
	}
	
	// Compile a given child of the given node
	private Object compileChild(SimpleNode node, int childIndex, Object data) {
		return node.jjtGetChild(childIndex).jjtAccept(this, data);
	}
	
	// Compile all children of the given node
	private Object compileChildren(SimpleNode node, Object data) {
		return node.childrenAccept(this, data);
	}

	// Get the ith child as a BaseASTNode
	private static BaseASTNode getChild(SimpleNode node, int childIndex) {
		return (BaseASTNode)node.jjtGetChild(childIndex);
	}
	
	public Object visit(SimpleNode node, Object data) {
		System.out.println(node + ": acceptor not implemented in subclass?");
		return data;
	}
	
	// Compile a Sili program, and return a ValueObject that can be executed by a VirtualMachine.
	public Object visit(ASTCode node, Object data) {
		// Compile all children of this node, which compiles the whole program.
		compileChildren(node, data);
		return null;
	}

	// Push integer literal to stack
	public Object visit(ASTInteger node, Object data) {
		return node.tokenValue;
	}

	// Push floating point literal to stack
	public Object visit(ASTRational node, Object data) {
		return node.tokenValue;
	}

	// Push true literal to stack
	public Object visit(ASTTrue node, Object data) {
		return true;
	}

	// Push false literal to stack
	public Object visit(ASTFalse node, Object data) {
		return false;
	}

	@Override
	public Object visit(ASTSchema node, Object data) {
		String path = (String)compileChild(node, 1, data);
		processor = new DataProcessor(path, this.outputFilename);
		
		List<String> keys = (List<String>)compileChild(node, 0, data);
		List<String> values = (List<String>)compileChild(node, 2, data);
		
		if(keys.size() != values.size()) {
			System.out.println("The quantity of keys does not match the number of values:" + keys.size() + ":" + values.size());
			return null;
		}

		processor.LoadData(
				(String[]) keys.toArray(new String[keys.size()]), 
				(String[]) values.toArray(new String[values.size()]));	
		return data;
	}

	@Override
	public Object visit(ASTPath node, Object data) {
		return node.tokenValue;
	}

	@Override
	public Object visit(ASTArgs node, Object data) {
		List<String> args = new ArrayList<String>();
		for(int i = 0; i < node.jjtGetNumChildren(); i++) {
			args.add((String)compileChild(node, i, data));
		}
		return args;
	}

	@Override
	public Object visit(ASTFunction node, Object data) {
	    String methodName = (String) compileChild(node, 0, data);
	    List<String> vars = (List<String>) compileChild(node, 1, data);
	    
		if(node.jjtGetNumChildren() >= 3) {
			compileChild(node, 2, new FunctionPayload(methodName, vars));
		}
		else {
			if(methodName.equalsIgnoreCase("display")) {
				processor.write(vars);
			}
		}
		return null;
	}

	// Function bodies are being used to compile JSON object into additional data sets.
	@Override
	public Object visit(ASTFunctionBody node, Object data) {
		compileChildren(node, data);
		return data;
	}

	//Returns an value which will be used to conduct an operation
	@Override
	public Object visit(ASTOperator node, Object data) {
		return compileChild(node, 0, data);
	}

	@Override
	public Object visit(ASTAddition node, Object data) {
		return new OperationAddition();
	}

	@Override
	public Object visit(ASTSubtract node, Object data) {
		return new OperationSubtract();
	}

	@Override
	public Object visit(ASTMultiply node, Object data) {
		return new OperationMultiplication();
	}

	@Override
	public Object visit(ASTExponent node, Object data) {
		return new OperationExponent();
	}

	@Override
	public Object visit(ASTLog node, Object data) {
		return new OperationLog();
	}

	@Override
	public Object visit(ASTString node, Object data) {
		return node.tokenValue;
	}

	@Override
	public Object visit(ASTDivide node, Object data) {
		return new OperationDivision();
	}

	@Override
	public Object visit(ASTMin node, Object data) {
		return new OperationMin();
	}

	@Override
	public Object visit(ASTMax node, Object data) {
		return new OperationMax();
	}

	@Override
	public Object visit(ASTAvg node, Object data) {
		return new OperationAvg();
	}

	@Override
	public Object visit(ASTSum node, Object data) {
		return new OperationSum();
	}

	@Override
	public Object visit(ASTRange node, Object data) {
		return new OperationRange();
	}

	@Override
	public Object visit(ASTReserved node, Object data) {
		System.out.println("Reserved keyword used in place of a literal.");
		System.exit(1);
		return null;
	}

	@Override
	public Object visit(ASTAggregate node, Object data) {
		return compileChild(node, 0, data);
	}

	@Override
	public Object visit(ASTMath node, Object data) {
		compute(node, data);
		return data;
	}

	private Operand GrabOperands(String key) {
		// Check for a numeric strings
		try {
			float t = Float.parseFloat(key);			
			return new Operand(t);
		}
		catch (NumberFormatException e) {
			return processor.getVariable(key);
		}
	}

	@Override
	public Object visit(ASTaggFunction node, Object data) {
		FunctionPayload payload = (FunctionPayload) data;
		
		String Operand1 = (String) compileChild(node, 0, data);
		Operation Operator = (Operation) compileChild(node, 1, data);
		
		if(!payload.getVariable().contains(Operand1)) {
			System.out.println("Agg: Operands do not match function definition");
		}

		//Acquire Lists for processing
		Operand operators1 = GrabOperands(Operand1);
		
		//Pass whole lists if the operator is aggregate
		processor.setVariable(payload.getName(), Operator.Compute(operators1, null));
		return null;
	}

	@Override
	public Object visit(ASTLogic node, Object data) {
		return this.compileChild(node, 0, data);
	}

	@Override
	public Object visit(ASTGT node, Object data) {
		return new OperationGT();
	}

	@Override
	public Object visit(ASTGTE node, Object data) {
		return new OperationGTE();
	};

	@Override
	public Object visit(ASTLT node, Object data) {
		return new OperationLT();
	}

	@Override
	public Object visit(ASTLTE node, Object data) {
		return new OperationLTE();
	}

	@Override
	public Object visit(ASTE node, Object data) {
		return new OperationEqual();
	}

	@Override
	public Object visit(ASTjoinFunction node, Object data) {
		FunctionPayload payload = (FunctionPayload) data;
		
		List<String> variableNames = new ArrayList<String>();
		for(int i = 0; i < node.jjtGetNumChildren(); i++) {
			String e = (String) this.compileChild(node, i, data);
			if(e.equals("|")) {
				continue;
			}
			variableNames.add(e);
		}
		
		List<Float> f = new ArrayList<Float>();
		for(String s : variableNames) {
			f.addAll((Collection<? extends Float>) processor.getVariable(s).values);
		}
		
		processor.setVariable(payload.getName(), new Operand(f));
		return data;
	}

	@Override
	public Object visit(ASTWhereFunction node, Object data) {
		compute(node, data);
		return data;
	}

	@Override
	public Object visit(ASTPipe node, Object data) {
		return node.tokenValue;
	}
	
	private void compute(SimpleNode node, Object data) {
		FunctionPayload payload = (FunctionPayload) data;
		
		String Operand1 = (String) compileChild(node, 0, data);
		String Operand2 = (String) compileChild(node, 2, data);
		Operation Operator = (Operation) compileChild(node, 1, data);
		
		if( (!payload.getVariable().contains(Operand1) && !IsInt(Operand1)) 
			|| (!payload.getVariable().contains(Operand2) && !IsInt(Operand2)) ) {
			System.out.println("Operands do not match function definition");
		}
		
		//Acquire Lists for processing
		Operand operators1 = GrabOperands(Operand1);
		Operand operators2 = GrabOperands(Operand2);
		
		//Pass whole lists if the operator is aggregate
		processor.setVariable(payload.getName(), Operator.Compute(operators1, operators2));
	}
	
	public boolean IsInt(String test) {
		int i;
		try {
			i = Integer.parseInt(test);
			return true;
		}
		catch(RuntimeException e) {
			//In the case this fails to parse we'll return false;
			return false;
		}
	}
}
