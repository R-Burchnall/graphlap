package uk.ac.derby.ldi.graphlab.operations;

import uk.ac.derby.ldi.graphlab.values.Operand;

//Interface type to allow handling of multiple operations
public interface Operation {
	 
	//Method used to conduct operation
	public Operand Compute(Operand operands1, Operand operands2);
	
	//Allows return of the string representation of the operation
	public String getOperator();
}
