package uk.ac.derby.ldi.graphlab.operations;

import java.util.ArrayList;
import java.util.List;

import uk.ac.derby.ldi.graphlab.values.Operand;

public class OperationAvg implements Operation{
	
	public Operand Compute(Operand operands1, Operand operands2) {
		float avg = 0;
		for(float f : operands1.values) {
			avg += f;
		}
		
		Operand o = new Operand(avg / operands1.values.size());
		return o;
	}

	public String getOperator() {
		return "AVG";
	}

	private int compareItems(final float item1, final float item2) {
		return (int) (item1 - item2);
	}
}