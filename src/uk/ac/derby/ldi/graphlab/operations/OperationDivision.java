package uk.ac.derby.ldi.graphlab.operations;

import java.util.ArrayList;
import java.util.List;

import uk.ac.derby.ldi.graphlab.values.Operand;

public class OperationDivision  implements Operation{
	
	public Operand Compute(Operand operand1, Operand operand2) {
		Operand o = new Operand();
		
		for (int i = 0; i < (operand1.values.size() > operand2.values.size() ? operand1.values.size() : operand2.values.size()); i++) {
			float x = operand1.isScalar ? operand1.values.get(0) : operand1.values.get(i);
			float y = operand2.isScalar ? operand2.values.get(0) : operand2.values.get(i);

			o.values.add(x / y);
		}
		
		return o;
	}

	public boolean isAggregate() {
		return false;
	}

	public String getOperator() {
		return "/";
	}

}
