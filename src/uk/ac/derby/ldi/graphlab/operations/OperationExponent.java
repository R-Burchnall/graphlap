package uk.ac.derby.ldi.graphlab.operations;

import java.util.ArrayList;
import java.util.List;

import uk.ac.derby.ldi.graphlab.values.Operand;

public class OperationExponent implements Operation{
	
	public Operand Compute(Operand operand1, Operand operand2) {
		List<Float> results = new ArrayList<Float>();
		for (int i = 0; i < operand1.values.size(); i++) {
			float x = operand1.isScalar ? operand1.values.get(0) : operand1.values.get(i);
			float y = operand2.isScalar ? operand2.values.get(0) : operand2.values.get(i);
			
			float temp = (float) Math.pow(x, y);
			if(Float.isNaN(temp)) {
				System.out.println("Warning: Decimal precision runout.");
				temp = 0;
			}
			results.add(temp);
		}
		return new Operand(results);
	}

	public String getOperator() {
		return "^";
	}

}