package uk.ac.derby.ldi.graphlab.operations;

import java.util.ArrayList;
import java.util.List;

import uk.ac.derby.ldi.graphlab.values.Operand;

public class OperationLog implements Operation{
	
	public Operand Compute(Operand operand1, Operand operand2) {
		List<Float> results = new ArrayList<Float>();
		for (int i = 0; i < operand1.values.size(); i++) {
			float temp = (float) Math.log(operand1.values.get(i));
			if(Float.isNaN(temp)) {
				System.out.println("Warning: Decimal precision runout.");
				temp = 0;
			}
			results.add(temp);
		}
		return new Operand(results);
	}

	public boolean isAggregate() {
		return false;
	}

	public String getOperator() {
		return "£$";
	}

}