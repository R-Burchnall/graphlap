package uk.ac.derby.ldi.graphlab.operations;

import java.util.ArrayList;
import java.util.List;

import uk.ac.derby.ldi.graphlab.values.Operand;

public class OperationMax implements Operation{
	
	public Operand Compute(Operand operand1, Operand operand2) {		
		float value = operand1.values.stream().max(this::compareItems)
				.orElse(Float.NEGATIVE_INFINITY);		
		return new Operand(value);
	}

	public String getOperator() {
		return "MAX";
	}

	private int compareItems(final float item1, final float item2) {
		return (int) (item1 - item2);
	}
}