package uk.ac.derby.ldi.graphlab.operations;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import uk.ac.derby.ldi.graphlab.values.Operand;

public class OperationRange implements Operation{
	
	public Operand Compute(Operand operand1, Operand operand2) {
		List<Float> results = new ArrayList<Float>();
		
		List<Float> value = operand1.values.stream().sorted(this::compareItems)
				.collect(Collectors.toList());
		if(value.isEmpty())
		{
			throw new IllegalStateException("List is empty, cannot get range.");
		}
				
		return new Operand(value.get(value.size()-1) - value.get(0));
	}

	public String getOperator() {
		return "RANGE";
	}

	private int compareItems(final float item1, final float item2) {
		return (int) (item1 - item2);
	}
}