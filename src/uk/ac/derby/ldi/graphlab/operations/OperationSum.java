package uk.ac.derby.ldi.graphlab.operations;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

import uk.ac.derby.ldi.graphlab.values.Operand;

public class OperationSum  implements Operation{
	
	public Operand Compute(Operand operand1, Operand operand2) {
		float total = 0;
		for (float f : operand1.values) {
			total += f;
		}

		return new Operand(total);
	}

	public String getOperator() {
		return "SUM";
	}
}