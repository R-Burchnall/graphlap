package uk.ac.derby.ldi.graphlab.values;

import java.util.*;

public class Operand {
	public boolean isScalar;
	public List<Float> values;
	
	public Operand() {
		this.values = new ArrayList<Float>();
		isScalar = true;
	}
	
	public Operand(List<Float> values) {
		this.values = values;
		isScalar = false;
	}
	
	public Operand(float value) {
		values = new ArrayList<Float>();
		values.add(value);
		isScalar = true;
	}
}
